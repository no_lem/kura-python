#!/usr/bin/env python

import OSC 
import time
import sys 
sys.path.insert(0, '/Users/nolanlem/kura/kura-python/config')
import config  

# read the config file 
'''
config.py looks like....
config = dict(
    sr = 8000,
    numoscs = 100,
    mean = 0.1,
    std = 0.1,
    kn_start = 0.05,
    kn_end = 0.2, 
    seconds = 20   
)
'''

client = OSC.OSCClient()
client.connect( ( '127.0.0.1', 57120))
msg = OSC.OSCMessage()
msg.setAddress("/config")


# get intializations from config file 
sr = config.config['sr']
msg.append(sr)

numoscs = config.config['numoscs']
msg.append(numoscs)

mean = config.config['mean']
msg.append(mean)

std = config.config['std']
msg.append(std)

kn = config.config['kn_start']
msg.append(kn)

kn_end = config.config['kn_end']
msg.append(kn_end)

seconds = config.config['seconds']
msg.append(seconds)


#params = []
#keys = []
#values = []
#for key, value in config.config.items():
#    keys.append(key)
#    values.append(value)
#    params.append({key: value})


    
#msg.setAddress("/config")
#for i in range(len(config.config)):
#    msg.append(keys[i])    
#    msg.append(params[i][keys[i]])


    
#msg.append()
client.send(msg)
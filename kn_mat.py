#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 16:31:47 2019

@author: nolanlem
"""

groups = 3 # vertical size, rows
rows = 6   # horiz size, cols
KN = np.empty((groups, rows)) # the kn matrix

for c in range(groups):
    val = np.random.rand()*1; 
    print c, val
    row = np.empty(rows)
    row.fill(val)
    KN[c,:] = row
    #print "filling row %r with constant rand val: %r" %(c, val)
    x
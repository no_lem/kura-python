#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 14:15:50 2019
about seaborn colors 
@author: nolanlem
"""

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
sns.set()


current_palette = sns.color_palette()
sns.palplot(current_palette)
#!/usr/bin/env python2

import numpy as np
from scipy.signal import hilbert, chirp
import matplotlib.pyplot as plt
import librosa

#fs = 600.0 #sampling frequency
#duration = 1.0 #duration of the signal
#t = np.arange(int(fs*duration)) / fs #time base

#a_t =  1.0 + 0.7 * np.sin(2.0*np.pi*3.0*t)#information signal
#c_t = chirp(t, 20.0, t[-1], 80) #chirp carrier
#x = a_t * c_t #modulated signal

sr = 8000
y, sr = librosa.load("../samples/operasing-mono.wav", sr=sr, duration=10)

plt.subplot(2,1,1)
plt.plot(y) #plot the modulated signal

z= hilbert(y) #form the analytical signal
inst_amplitude = np.abs(z) #envelope extraction
inst_phase = np.unwrap(np.angle(z))#inst phase
wrapped_phase = inst_phase%(2*np.pi) # for plotting

inst_freq = np.diff(inst_phase)/(2*np.pi)*fs #inst frequency

#Regenerate the carrier from the instantaneous phase
regenerated_carrier = 0.6*np.cos(inst_phase)

plt.plot(inst_amplitude,'r'); #overlay the extracted envelope
plt.title('Modulated signal and extracted envelope')
plt.xlabel('n')
plt.ylabel('x(t) and |z(t)|')
plt.subplot(4,1,2)
plt.plot(regenerated_carrier)
plt.title('Extracted carrier or TFS')
plt.xlabel('n')
plt.ylabel('cos[\omega(t)]')
plt.subplot(4,1,3)
plt.plot(inst_phase);
plt.xlabel("samples")
plt.ylabel('phase')
plt.subplot(4,1,4)
plt.plot(inst_freq)
plt.title('inst freq')

librosa.output.write_wav("../samples/rendered/regenerated_voice_hilbert.wav", regenerated_carrier, sr=sr);

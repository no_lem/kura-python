#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 16:53:51 2019

@author: nolanlem
"""

import numpy as np
import matplotlib.pyplot as plt
import random 


'''
random.vonmisesvariate(mu, kappa)
mu is the mean angle, expressed in radians between 0 and 2*pi, 
and kappa is the concentration parameter, which must be greater 
than or equal to zero. If kappa is equal to zero, this distribution
 reduces to a uniform random angle over the range 0 to 2*pi.
'''

numoscs = 1000
#x = np.linspace(0, 2*np.pi, 100, endpoint=False)
x = np.random.randn(numoscs)
y = np.linspace(0, 2*np.pi, numoscs, endpoint=False)
n = []
mu = np.pi # sets where the mean is on the circle [0, 2pi]
kappa = 50 # sets density amid mu, lower value is more spread (0=uniform distribution around 0-2pi)
[n.append(random.vonmisesvariate(mu,kappa)) for x in range(numoscs)]

marks = np.empty(numoscs)
marks.fill(100)

n = np.array(n)
xcirc = np.exp(1j*n)

fig = plt.figure(figsize=(5,5))
ax = fig.add_subplot(111)
#plt.scatter(xcirc.real, xcirc.imag, facecolors='none', edgecolors='r', s=marks, alpha=0.1)
plt.scatter(xcirc.real, xcirc.imag, facecolors='none', edgecolors='r', s=marks, alpha=0.1)
ax.plot(np.sin(np.linspace(0,2*np.pi,100)), np.cos(np.linspace(0,2*np.pi,100)), linewidth='0.5', alpha=1.0)
ax.set_xbound(lower=-1.2, upper=1.2)
ax.set_ybound(lower=-1.2, upper=1.2)


plt.show()


#ax = plt.subplot(111, polar=True)
#bars = ax.bar(axis, data, width=2*np.pi/100, bottom = 1)
#plt.show()
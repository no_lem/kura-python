#!/usr/bin/env python

import numpy as np
import librosa 
from librosa import display 
import sys


def frame2samp(frames, hop):
    # linear interpolation 

    samples = np.zeros((frames.shape[0], frames.shape[1]*hop))

    for i in range(frames.shape[0]):
        for j in range(1,frames.shape[1]):
            samples[i,(j-1)*hop:(j*hop)] = np.linspace(frames[i,j-1], frames[i,j], hop)
    return samples 

"""
def phase_vocoder(D, rate, hop_length=None):
    Phase vocoder.  Given an STFT matrix D, speed up by a factor of `rate`

    Based on the implementation provided by [1]_.

    .. [1] Ellis, D. P. W. "A phase vocoder in Matlab."
        Columbia University, 2002.
        http://www.ee.columbia.edu/~dpwe/resources/matlab/pvoc/

    Examples
    --------
    >>> # Play at double speed
    >>> y, sr   = librosa.load(librosa.util.example_audio_file())
    >>> D       = librosa.stft(y, n_fft=2048, hop_length=512)
    >>> D_fast  = librosa.phase_vocoder(D, 2.0, hop_length=512)
    >>> y_fast  = librosa.istft(D_fast, hop_length=512)

    >>> # Or play at 1/3 speed
    >>> y, sr   = librosa.load(librosa.util.example_audio_file())
    >>> D       = librosa.stft(y, n_fft=2048, hop_length=512)
    >>> D_slow  = librosa.phase_vocoder(D, 1./3, hop_length=512)
    >>> y_slow  = librosa.istft(D_slow, hop_length=512)

    Parameters
    ----------
D : np.ndarray [shape=(d, t), dtype=complex]
    STFT matrix

rate :  float > 0 [scalar]
    Speed-up factor: `rate > 1` is faster, `rate < 1` is slower.

hop_length : int > 0 [scalar] or None
    The number of samples between successive columns of `D`.

    If None, defaults to `n_fft/4 = (D.shape[0]-1)/2`

Returns
-------
D_stretched  : np.ndarray [shape=(d, t / rate), dtype=complex]
    time-stretched STFT
"""  

sr = 8000
sec = 8.0
t = np.arange(0, sr*sec, 1)
freq = 1000
env = np.linspace(0,1.0,sr*sec)

harms = 20
sig = np.zeros((harms, int(sr*sec)))

for i in range(harms):
    sig[i, :] = np.sin(2*np.pi*freq*i*t/sr)

sig = np.sum(sig, axis=0)
sig /= harms

#sig = 0.8*np.sin(2*np.pi*freq*t/sr) 

N = 256 # DFT size
rate = 1.0 # phase vocoder synthesis rate

hop_length = N // 4 # hop size

filetoload = "../samples/flute8k.wav"
sig, s = librosa.load(filetoload, duration=sec, sr=sr)
#D = librosa.stft(y, n_fft=N)
D = librosa.stft(sig, n_fft=N, win_length=N, hop_length=hop_length) # these are defaults in stft def

# D shape = (N/2, samples/(hop_length))  

print 'phase vocoder for rate = %r' %(rate) 
#dstretch, dp = phase_vocoder(D, 0.5)


print 'D shape: ', D.shape
n_fft = 2 * (D.shape[0] - 1) # length of stft, N, 
print "n_fft length:", n_fft

if hop_length is None:
    hop_length = int(n_fft // 4)
    

print 'hop_length is', hop_length

time_steps = np.arange(0, D.shape[1], rate, dtype=np.float) # rate * number of frames in stft
print 'time_steps shape: ', time_steps.shape

# Create an empty output array
# size of new spectrum that is N, stretched/compressed frames (complex array of zeros)
d_stretch = np.zeros((D.shape[0], len(time_steps)), D.dtype, order='F')
print 'd_strech shape: ', d_stretch.shape 

# Expected phase advance in each bin
# vector to hold phases per bin
# goes from 0 --> 2*pi*(N/4) if hop_length is N/4
# hop leng
phi_advance = np.linspace(0, np.pi * hop_length, D.shape[0])
#plt.plot(phi_advance)
print 'phi_advance shape', phi_advance.shape 

# Phase accumulator; initialize to the first sample
# (N, ) => (1025,)
phase_acc = np.angle(D[:, 0])
print 'phase_acc shape: ', phase_acc.shape 

# Pad 0 columns to simplify boundary logic
# add 0s to front and back side of frames 
D = np.pad(D, [(0, 0), (0, 2)], mode='constant')
print 'D shape: ', D.shape


all_phases = np.zeros((D.shape[0], len(time_steps)))
all_mags = np.zeros((D.shape[0], len(time_steps)))
pdevs = np.zeros((D.shape[0], len(time_steps))) 

for (t, step) in enumerate(time_steps): # (0,0.0),(1, 0.5)

    columns = D[:, int(step):int(step + 2)] # (:, [0][1])
    #print 'columns: ', columns

    # Weighting for linear magnitude interpolation
    alpha = np.mod(step, 1.0) # alpha alternates 0 and 0.5 for rate = 0.5
    mag = ((1.0 - alpha) * np.abs(columns[:, 0])
            + alpha * np.abs(columns[:, 1]))

    # Store to output array
    d_stretch[:, t] = mag * np.exp(1.j * phase_acc)
    all_phases[:, t] = phase_acc
    all_mags[:, t] = mag
    #print 'd_stretch', d_stretch

    # Compute phase advance
    dphase = (np.angle(columns[:, 1])
              - np.angle(columns[:, 0])
              - phi_advance)

    
    #print '\n dphase ', dphase 
    # Wrap to -pi:pi range
    dphase = dphase - 2.0 * np.pi * np.round(dphase / (2.0 * np.pi))
    pdevs[:,t] = dphase/hop_length
    #print '\n dphase wrapped ', dphase
    # Accumulate phase
    phase_acc += phi_advance + dphase
    #print '\n phase_acc ', phase_acc

#    return d_stretch, dphase

bindiv = sr/N # bin division in Hz
fbins = bindiv*np.arange(0,N/2,1) # frequency of each bin (N/2)
wkbins = fbins*2*np.pi/sr   # rad freq of each bin (N/2)
# these are instantaneous frequencies with delta phase added and hop_
wk_delta = pdevs + phi_advance[:, np.newaxis]/hop_length # still at frame level 

ifreq_samps = frame2samp(wk_delta, hop_length)
mag_samps = frame2samp(all_mags, hop_length)

# just for producing freq vector with kura-synthesis-pvoc.py
# then you need to manually select the columns (freqs) you want, has to match number 
# of oscillators in config.py file 
freq = np.copy(ifreq_samps[:, :-hop_length])
mags = np.copy(mag_samps[:, :-hop_length])

sins = np.zeros((freq.shape[0], freq.shape[1])) 
sins = mags*np.sin(freq*sr)


# recreate instantaneous phase per frame across samples 
pvect = np.zeros((all_phases.shape[0], hop_length*all_phases.shape[1]))
mvect = np.zeros((all_mags.shape[0], hop_length*all_mags.shape[1]))
sins = np.zeros((pvect.shape[0], pvect.shape[1])) 

for i in range(all_phases.shape[0]):
    for k in range(1, all_phases.shape[1]):
        pvect[i,(k-1)*hop_length:(k*hop_length)] = np.linspace(all_phases[i,k-1], all_phases[i,k], hop_length) 
        mvect[i, (k-1)*hop_length:(k*hop_length)] = np.linspace(all_mags[i,k-1], all_mags[i,k], hop_length)         
    sins[i, :] = mvect[i,:]*np.sin(pvect[i,:])

    
sins = np.sum(sins, axis=0)
sins = 0.8*sins/max(sins)
 
librosa.output.write_wav("resynth-sine.wav", sins, sr)
    
     

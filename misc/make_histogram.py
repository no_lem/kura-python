#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 10:41:59 2019

@author: nolanlem
"""

import numpy as np
import matplotlib.pyplot as plt 
from collections import Counter
import pandas as pd
import sys
import seaborn as sns
sns.set_style('darkgrid')

numoscs = 1000
numbins = 25 
freqsteps = np.random.randn(numoscs)
mu = 0.3, 0.1
sigma = 0.2, 0.1
array_of_steps = np.random.normal(loc=mu, scale=sigma, size=(numoscs,2)) 
dist = pd.DataFrame(
        array_of_steps,
        columns=['a', 'b'])
dist.agg(['min','max','mean','std']).round(decimals=2)

fig, ax = plt.subplots()
dist.plot.kde(ax=ax, legend=False, title="histo a")
dist.plot.hist(density=True, ax=ax, bins=numbins)
ax.set_ylabel('probability')
ax.grid(axis='y')
sys.exit(-1)

first_edge = freqsteps.min()
last_edge = freqsteps.max() 


bins = np.linspace(first_edge, last_edge, num=numbins+1, endpoint=True)

histo, bin_edges = np.histogram(freqsteps, numbins)

# plot 

n, bins, patches = plt.hist(x=freqsteps, bins=bins, color='#0504aa',
                            alpha=0.7, rwidth=None, density=False)

plt.grid(axis='y', alpha=0.75)
plt.xlabel('value')
plt.ylabel('frequency')
plt.title('my very own histo')
plt.text(23, 45, r'$\mu, b=3$')

plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) *
                np.exp( - (bins - mu)**2 / (2 * sigma**2) ),
          linewidth=2, color='r')
maxfreq = n.max()
# set a clean upper y-axis limit 
plt.ylim(ymax=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)


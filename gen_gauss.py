#!/ usr/bin/env python

# usage: just create gaussian distribution for oscillator intrinsic frequencies 
# and generate newline delimited txt file to be read by supercollider or chuck 
# for coupled oscillator program

import numpy as np
import matplotlib.pyplot as plt 

numoscs = 110; 
mu, sigma = 0.1, 0.1 # mean and std deviation 
dist = np.random.normal(mu, sigma, numoscs);
dist += 0.1;
# sanity check the mean and variance 
abs(mu - np.mean(dist)) < 0.01 
abs(sigma - np.std(dist, ddof=1)) < 0.01 

# generate histogram
count, bins, ignored = plt.hist(dist, 30, density=True)
plt.plot(bins, 1/(sigma*np.sqrt(2*np.pi))*np.exp(-(bins-mu)**2/(2*sigma**2)),
         linewidth=2, color='r')

plt.show()

filename = "init_freqs/gaussian_mean-"+str(mu) + "_dev-"+str(sigma) + ".txt"
f = open(filename, "w")

for freq in dist:
    f.write(str(freq) + "\n")
    
dist
f.close()
    
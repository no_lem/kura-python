#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import time
sys.path.insert(0, '/Users/nolanlem/Documents/kura/kura-python/config')
import config 
import pandas as pd


sns.set()

pi = np.pi

current_palette = sns.color_palette()
sns.palplot(current_palette)


def gaussian(numoscs, mu=0.00, sigma=0.1):
    #mu, sigma = 0.00, 0.1 # mean and std deviation 
    dist = np.random.normal(mu, sigma, numoscs);
    dist += 0.1;
    # sanity check the mean and variance 
    abs(mu - np.mean(dist)) < 0.01 
    abs(sigma - np.std(dist, ddof=1)) < 0.01 
    
    # generate histogram
    count, bins, ignored = plt.hist(dist, 30, density=True)
    plt.plot(bins, 1/(sigma*np.sqrt(2*np.pi))*np.exp(-(bins-mu)**2/(2*sigma**2)),
         linewidth=2, color='r')
    
    plt.show()
    return dist

def makeHistogram(intitial_freqs, evolved_freq, numbins, iters): 

    dist1 = pd.DataFrame(
            initial_freqs,
            columns=['intrinsic frequency distribution'])
    dist1.agg(['min','max','mean','std']).round(decimals=2)
    
    dist2 = pd.DataFrame(
            evolved_freq,
            columns=['entrained frequency distribution'])
    #dist2.agg(['min','max','mean','std']).round(decimals=2)
    
    fig1, ax1 = plt.subplots()
    dist1.plot.kde(ax=ax1, legend=False, title="histogram of intrinsic frequencies")
    dist1.plot.hist(normed=True, ax=ax1, bins=numbins)
    ax1.set_ylabel('probability')
    ax1.set_xlabel('radian freq')
    
    fig2, ax2 = plt.subplots()
    #dist2.plot.kde(ax=ax2, legend=False, title="histogram of evolved freq")
    #dist2.plot.hist(ax=ax2, bins=numbins, weights=np.ones(len(evolved_freq))/len(evolved_freq), density=False)
    dist2.plot.hist(ax=ax2, bins=numbins, normed=True, color=current_palette[0], title="entrained frequency distributions after %r iterations"%(iters)) 
    #ax2.set_xlim(-0.2,0.6)
    ax2.set_ylabel('probability')
    return dist1, dist2

def populateCircleMap(thephases):
    marks = np.empty(numoscs)
    marks.fill(100)
    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot(111)
    plt.scatter(np.cos(thephases), np.sin(thephases), facecolors='none', edgecolors='r', s=marks, alpha=0.1)
    ax.plot(np.sin(np.linspace(0,2*np.pi,100)), np.cos(np.linspace(0,2*np.pi,100)), linewidth='0.5', alpha=1.0)
    ax.set_xbound(lower=-1.2, upper=1.2)
    ax.set_ybound(lower=-1.2, upper=1.2)

# helper functions 
def accumulateR(ph, numoscs):
#    function to  complex order params   
#    for i in range(numoscs):
#        if (ph[i] >= 2*pi):
#            ph[i] = ph[i]%(2*pi) # constrain limit cycle
#        if (ph[i] <= -2*pi):
#            ph[i] = ph[i]%(-2*pi) # constrain limit cycle
        #    "trigger osc[%r]" %(i)
        # accumulate complex order param
        # r = r + np.exp(1j*ph[i])
    
    # accumulate complex order parameter, r
    # r = 0
    r = np.sum(np.exp(ph*1j))   
    r = r/numoscs # divide by number of active oscillators
    rang = np.angle(r) # get avg phase angle, rang
    rmag = np.abs(r) # get phase coherence, rmag
    return [rang, rmag] # return complex order params

def updatePhases(ph, pang, orderparams, kn):
    # function to update phases of each oscillator in ensemble
    rang, rmag = orderparams
#    for i in range(numoscs):
#        p_ang = kn*rmag*np.sin(rang - ph[i])
#        ph[i] = ph[i] + freq[i] + p_ang
    pang = kn*rmag*np.sin(rang-ph)   
    ph = ph + freq + pang
    # return the phases
    return ph 
 
    

# freq of oscillators 
#fixedfreq = 0.5
#freq = np.empty(numoscs)
#freq.fill(fixedfreq) # freq is the intrinsic freq of each osc
# my initialized phases


# how many oscillators in ensemble
numoscs = config.config['numoscs']
p = 2*pi*np.random.rand(numoscs) # random initial phases
pang = np.zeros(numoscs)
#p = np.random.rand(numoscs)*0.0
kn = config.config['kn_start']
kn_end = config.config['kn_end'] 
mean = config.config['mean']
std = config.config['std']
kn_to_span = kn_end - kn

# sanity check from config.py file 
print("PARAMETERS: \n numoscs: {} kn: %r \n kn_end: %r mean: %r std: %r".format(numoscs, kn, kn_end, mean, std)) 
 
#freq = gaussian(numoscs, mean, std)
freq = np.random.normal(mean, std, numoscs)
initial_freqs = np.copy(freq)

phases = []
# how many iterations for one cycle at freq = 0.1 
# = 2*pi/0.1
# get mean cycle period and fun for iters times
cycle = 2*np.pi/mean
iters = 100
iterations = iters*cycle
totaliters = np.int(iterations)
print("running for %r iterations".format(totaliters))

# to time loops 
avgdur = 0.0

snapshot_freqs = []
snapshot_phases = []
rmag = []
rang = []
phases_rang = []
snapshot_time = int(np.floor(cycle))

totaliters = 1000
# main loop 
for x in range(totaliters):
    print("computing %r/%r iteration \r"%(x, totaliters))
   # sys.stdout.write('\r' + '%r/%r iteration'%(x,totaliters))
   # sys.stdout.flush()
    p_old = np.copy(p)
    #start = time.time()
    complexorderparams = accumulateR(p, numoscs)
    r_ang = complexorderparams[0]
    r_mag = complexorderparams[1]
    rmag.append(r_mag)
    rang.append(r_ang)
    
    p = updatePhases(p, pang, complexorderparams, kn)

    #print 'p_old: %r \n p: %r \n p_old-p: %r'%(p_old,p, p-p_old)
    p_ = p.tolist()
    phases.append(p_)
    phases_rang.append(p-r_ang)
    #stop = time.time() 
    #duration = stop-start
    #avgdur += duration 
    kn += kn_to_span/totaliters
    
    if x%snapshot_time == 0:
        snapshot_phases.append(p)
    
    dp_dt = p - p_old
    snapshot_freqs.append(dp_dt)

plt.subplots()
plt.plot(rmag, linewidth='0.5')
plt.title('phase coherence: %r iterations'%(totaliters))
plt.xlabel('iteration')
plt.ylabel('R')

#print snapshot_freqs
    # make HISTOGRAM
#snapshot_freqs = np.array(snapshot_freqs)
#snapshot_freqs = snapshot_freqs.flatten() # flatten it
#numbins = 25
#dist1, dist2 = makeHistogram(initial_freqs, snapshot_freqs, numbins)

#entrained_freqs = dist2.get("entrained frequency distribution")

#snapshot_phases = np.array(snapshot_phases) #(frames, numoscs)
#phases = np.exp(1j*snapshot_phases)
#plt.subplots()
#plt.plot(entrained_freqs.real, entrained_freqs.imag)
#ax = plt.gca()
#ax.set_aspect('equal', 'box')
    
# make HISTOGRAM
numbins = 50
snapshot_freqs = np.array(snapshot_freqs)
snapshot_freqs = snapshot_freqs.flatten() # flatten it
dis1, dist2 = makeHistogram(initial_freqs, snapshot_freqs, numbins, totaliters)

entrained_freqs = dist2.get("entrained frequency distribution")

#make circle density histo 
#prang = np.array(phases_rang)
#plt.figure()

# plot the phases minus the average phase, should converge to near zero over time 
#plt.plot(prang[:,10:25]%(2*np.pi))

#populateCircleMap(prang)

#print("iter %r took %r seconds")%(x,duration)

#print "with %r oscillators in ensemble, the average time of each loop iteration was: %r" %(numoscs, avgdur/iterations)    
#    
#plt.figure()
#plt.plot(phases)
#plt.title("instantaneous phases of oscillators in ensemble")
#plt.xlabel("time, iter")
#plt.ylabel("phase")  


# with 10 oscillators in ensemble, the average time of each loop iteration was: 0.00028000593185424806
# with 144 oscillators in ensemble, the average time of each loop iteration was: 0.0011337089538574218
# with 200 oscillators in ensemble, the average time of each loop iteration was: 0.0020474290847778322



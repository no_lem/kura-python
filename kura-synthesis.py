#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 16:16:58 2019

@author: nolanlem
"""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import time
import datetime
import librosa
import sys
sys.path.insert(0, '/Users/nolanlem/Documents/kura/kura-python/config')
import config 
from scipy.signal import hilbert, chirp


sns.set()
pi = np.pi


timestamp=datetime.datetime.fromtimestamp(time.time()).strftime('%H-%M-%S')

def freq2rad(freqs, sr):
    freqs = freqs*2*np.pi/sr
    return freqs

def frame2samp(frames, hop):
    # linear interpolation 
    samples = np.zeros((frames.shape[0], frames.shape[1]*hop))
    for i in range(frames.shape[0]):
        for j in range(1,frames.shape[1]):
            samples[i,(j-1)*hop:(j*hop)] = np.linspace(frames[i,j-1], frames[i,j], hop)
    return samples 
 
# create gaussian for intrinsic freq
def gaussian(numoscs, filename, mu=0.00, sigma=0.1):
    #mu, sigma = 0.00, 0.1 # mean and std deviation 
    dist = np.random.normal(mu, sigma, numoscs);
    # sanity check the mean and variance 
    abs(mu - np.mean(dist)) < 0.01 
    abs(sigma - np.std(dist, ddof=1)) < 0.01 
    
    # generate histogram
    count, bins, ignored = plt.hist(dist, 30, density=True)
    plt.figure()
    plt.plot(bins, 1/(sigma*np.sqrt(2*np.pi))*np.exp(-(bins-mu)**2/(2*sigma**2)),
        linewidth=2, color='r')
    plt.savefig(filename + ".png")
    #plt.show()
    return dist

def getHilbert(filename, sr, dur):
    y, sr = librosa.load(filename, sr=sr, duration=dur)
    z= hilbert(y) #form the analytic al signal
    inst_amplitude = np.abs(z) #envelope extraction
    inst_phase = np.unwrap(np.angle(z))#inst phase
    wrapped_phase = inst_phase%(2*np.pi) # for plotting

    inst_freq = np.diff(inst_phase)/(2*np.pi)*sr #inst frequency

    #Regenerate the carrier from the instantaneous phase
    regenerated_carrier = 0.6*np.cos(inst_phase)
    
    if len(wrapped_phase) != totaliters:
        print "audio source is different length"
    if len(wrapped_phase) < totaliters:
        print '%r is less than than %r' %(len(wrapped_phase), totaliters)       
        samp_diff = totaliters - len(source_phase)
        np.pad(wrapped_phase, (0,samp_diff), 'constant', constant_values=(0))# main loop 
    if len(wrapped_phase) > totaliters:
        print '%r is greater than %r' %(len(wrapped_phase), totaliters)
        samp_diff = len(wrapped_phase) - totaliters 
        wrapped_phase = wrapped_phase[:totaliters]
    
    return wrapped_phase

def makeHistogram(intitial_freqs, evolved_freq, numbins): 

    dist1 = pd.DataFrame(
            initial_freqs,
            columns=['intrinsic frequency distribution'])
    dist1.agg(['min','max','mean','std']).round(decimals=2)
    
    dist2 = pd.DataFrame(
            evolved_freq,
            columns=['entrained frequency distribution'])
    #dist2.agg(['min','max','mean','std']).round(decimals=2)
    
    fig1, ax1 = plt.subplots()
    dist1.plot.kde(ax=ax1, legend=False, title="histogram of intrinsic freq")
    dist1.plot.hist(normed=True, ax=ax1, bins=numbins)
    ax1.set_ylabel('probability')
    
    fig2, ax2 = plt.subplots()
    #dist2.plot.kde(ax=ax2, legend=False, title="histogram of evolved freq")
    #dist2.plot.hist(ax=ax2, bins=numbins, weights=np.ones(len(evolved_freq))/len(evolved_freq), density=False)
    dist2.plot.hist(ax=ax2, bins=numbins, normed=True, color=current_palette[0]) 
    #ax2.set_xlim(-0.2,0.6)
    ax2.set_ylabel('probability')

# helper functions 
def accumulateR(ph, numoscs):
#    function to  complex order params 
    # r = 0
    r = np.sum(np.exp(ph*1j))   
    r = r/numoscs # divide by number of active oscillators
    rang = np.angle(r) # get avg phase angle, rang
    rmag = np.abs(r) # get phase coherence, rmag
    return [rang, rmag] # return complex order params

# this function was to check that system behavior is the same if we 
# forego turning the phases into a complex phasor
# e.g. we can just sum the phases and average them 
def sumAngle(ph, numoscs):
    rang = np.sum(ph)/numoscs
    return rang

    

def updatePhases(ph, freq, pang, orderparams, kn):
    # function to update phases of each oscillator in ensemble
    rang, rmag = orderparams
    pang = kn*rmag*np.sin(rang-ph) 
    ph = ph + freq + pang
    # return the phases
    return ph

def updatePhasesExt(ph, pang, orderparams, kn, ext_source, it):
    # function to update phases of each oscillator in ensemble
    rang, rmag = orderparams
    pang = kn*rmag*np.sin(rang-ph) 
    ph = ph + freq*ext_source[it] + pang
    # return the phases
    return ph


def updatePhasesAM(ph, amsig, am_step, pang, orderparams, kn, it):
    # function to update phases of each oscillator in ensemble
    rang, rmag = orderparams
    pang = kn*rmag*np.sin(rang-ph)
    amsig = (amsig + am_step)%(2*np.pi)
    #amsig = amsig
    #amsig = amsig
    ph = ph + freq + pang
    # return the phases
    return ph, amsig

def updatePhases_(ph, freq, rang, kn):
    # functison to update phases of each oscillator in ensemble
    pang = kn*np.sin(rang-ph) 
    ph = ph + freq + pang
    # return the phases
    return ph




# sampling rate
sr = config.config['sr'] # sr=8k

# how many oscillators in ensemble
numoscs = config.config['numoscs']
# freq of oscillators 

fixedfreq = 0.5
freq = np.empty(numoscs)
freq.fill(fixedfreq) # freq is the intrinsic freq of each osc
freq[0] = 200; 
freq[1] = 300; 
freq[2] = 400; 
freq[3] = 500; 
freq[4] = 600;
freq[5] = 700; 
freq[6] = 800; 
freq[7] = 900;  
# my initialized phases

    
freq = freq2rad(freq, sr=sr)

# or gaussian/normal distribution 
mean = config.config['mean'] 
std = config.config['std']



p = 2*pi*np.random.rand(numoscs) # random initial phases
p = np.random.rand(numoscs)*0.0
pang = np.zeros(numoscs)
kn = config.config['kn_start']
kn_end = config.config['kn_end'] 
mean = config.config['mean']
std = config.config['std']
kn_to_span = kn_end - kn
wavtitle = "%r oscs kn %r to %r mu %r dev %r"%(numoscs,kn,kn_end,mean,std)

#freq = gaussian(numoscs, wavtitle, mean, std)


# sanity check params
print 'PARAMETERS: \n numoscs: %r kn: %r \n kn_end: %r mean: %r std: %r' %(numoscs, kn, kn_end, mean, std) 

phases = []
# how many iterations for one cycle at freq = 0.1 
# = 2*pi/0.1
cycles = 2*pi/fixedfreq
seconds = config.config['seconds']
iterations = seconds*cycles
print "running for %r iterations" %(iterations)
samples = []
phasecoherence = []
avgphase = []
phases = []

avgdur = 0.0


totaliters = sr*seconds

# extract Analytical signal to get instantaneous phase from 
# external audio file 
#externalsource = "./samples/opera-8k.wav"
#duration = totaliters/float(sr)
#source_phase = getHilbert(externalsource, sr, duration)

#

# AM Mod
#lfmodhz = 10.0 
#amsig = np.zeros(numoscs)
#am_step = lfmodhz/sr

#if len(source_phase) != totaliters:
#    print "audio source is different length"
#    if len(source_phase) < totaliters:
#        print '%r is less than than %r' %(len(source_phase), totaliters)       
#        samp_diff = totaliters - len(source_phase)
#        np.pad(source_phase, (0,samp_diff), 'constant', constant_values=(0))# main loop 
#    if len(source_phase) > totaliters:
#        print '%r is greater than %r' %(len(source_phase), totaliters)
#        samp_diff = len(source_phase) - totaliters 
#        source_phase = source_phase[:totaliters]

for x in range(totaliters):
    print "computing iteration %r/%r" %(x, totaliters)
    start = time.time()
     
    complexorderparams = accumulateR(p, numoscs)
    phasecoherence.append(complexorderparams[1])
    avgphase.append(complexorderparams[0])
    
    #pang = sumAngle(p, numoscs)
    
    #def updatePhases(ph, freq, pang, orderparams, kn):
    p = updatePhases(p, freq, pang, complexorderparams, kn)
    #p = updatePhases_(p, freq, pang, kn)
    
    #p, amsig = updatePhasesAM(p, amsig, am_step, pang, complexorderparams, kn, x
    
    p_ = p.tolist()
    phases.append(p_)
    samples.append(np.cos(p_))
    
    stop = time.time() 
    
    duration = stop-start
    avgdur += duration 
    kn += kn_to_span/totaliters
    #print("iter %r took %r seconds")%(x,duration)

print "with %r oscillators in ensemble, the average time of each loop iteration was: %r" %(numoscs, avgdur/iterations)    
    
#plt.figure()
#plt.plot(samples)
#plt.title("instantaneous phases of oscillators in ensemble")
#plt.xlabel("time, iter")
#plt.ylabel("phase")  

phasecoherence = np.array(phasecoherence)
avgphase = np.array(avgphase)

plt.figure()
plt.plot(phasecoherence)
#plt.plot(avgphase)
#plt.savefig("./r-plots/" + wavtitle + timestamp + ".png")

#plt.show()


samples = np.array(samples) # form np array
summedsines = 0.5*np.sum(samples, axis=1)/numoscs # sum along sample columns


#print "writing %r.wav" %(wavtitle+timestamp)
#librosa.output.write_wav("./outputaudio/" + wavtitle + timestamp + ".wav", summedsines, sr)

librosa.output.write_wav("kura-synthesis.wav", summedsines, sr)


# datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
#>> '2019-01-23 16:59:12'

# with 10 oscillators in ensemble, the average time of each loop iteration was: 0.00028000593185424806
# with 144 oscillators in ensemble, the average time of each loop iteration was: 0.0011337089538574218
# with 200 oscillators in ensemble, the average time of each loop iteration was: 0.0020474290847778322

    


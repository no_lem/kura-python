#!/usr/bin/env python

# Array manipulation
from numpy import append, asarray, empty, ones, tile

from numbers import Number
import librosa 
from librosa import display

import matplotlib.pyplot as plt


# Mathematical
from numpy import abs, angle, imag, real
from numpy import cos, dot, exp, mean, mod, pi, sin, sum
from numpy.random import normal, uniform
import numpy as np

# Plotting
import matplotlib.pyplot as pyplot

class Kuramoto:
    def __init__(self, size, coupling, stepsize, phases=None, nat_frequencies=None, stepcount=0):
        self.size = size

        if isinstance(coupling, Number): # Uniform coupling
            self.coupling = ones(size)*coupling
        else:
            self.coupling = asarray(coupling)

        if self.coupling.size != size:
            raise ValueError("Not enough coupling strengths for every oscillator in the network.")

        if phases is None:
            self.phases = uniform(0, 2*pi, size)
        else:
            self.phases = asarray(phases)

        if self.phases.size != size:
            raise ValueError("Not enough initial phases for every oscillator in the network.")

        if nat_frequencies is None:
            self.nat_frequencies = normal(0 , fs/2, size)
        else:
            self.nat_frequencies = asarray(nat_frequencies)

#        if asarray(nat_frequencies).size != size:
#            raise ValueError("Not enough natural frequencies for every oscillator in the network.")

        self.stepsize = stepsize
        self.stepcount = stepcount

    def make_step(self, it, update=True):
# First order explicit forward Euler for now
        tiled = tile(self.phases, (self.size, 1)) # phases by phases (N,N)
        #print 'tiled shape', tiled
        #k1 = self.coupling * sum(sin(self.phases - tiled.T), axis=1)
        #k1 = dot(sin(self.phases - tiled.T), self.coupling)
        
        k1 = dot(sin(self.phases - tiled.T), kn_sig[:, it])

        #print 'k1 shape', k1
        if update:
            #self.phases += self.stepsize * (self.nat_frequencies + k1)
            self.phases += self.nat_frequencies[:, it] + k1
            #print 'self.phases', phases

# The modulo operation is strictly speaking not necessary; remove for a speedup.
            ph[:,it] = self.phases
            self.phases = mod(self.phases, 2*pi)
            self.stepcount += 1
        else:
            return k1

    def make_step_loop(self, update=True):
# First order explicit forward Euler using explicit, slow loops
        k1 = empty(self.size)
        for i in range(0, self.size):
            #k1[i] = self.coupling * sum(sin(self.phases - self.phases[i]))
            k1[i] = dot(sin(self.phases - self.phases[i]), self.coupling)

        if update:
            self.phases += self.stepsize * (self.nat_frequencies + k1)

# The modulo operation is strictly speaking not necessary; remove for a speedup.
            self.phases = mod(self.phases, 2*pi)
            self.stepcount += 1
        else:
            return k1

    def order_parameters(self):
        o = mean(exp(self.phases * complex(0,1)))
        return abs(o), angle(o)

    def rectangular(self):
        p = exp(self.phases * complex(0,1))
        return real(p), imag(p)

def run(model, steps):
    order_parameters = empty((2,steps))
    for i in range(steps):
        model.make_step(i)
        order_parameters[:,i] = model.order_parameters()
    return order_parameters


fs = 8000
seconds = 30

steps = fs*seconds
N = 10
#J = 0.5
K = 0.1
dt = 0.05

# vector to hold phases per sample
ph = np.zeros((N, steps))

# modulating coupling over time
tkn = np.arange(0,steps)
knfreq = 2.0/seconds
#kn_sig = 0.00525*np.sin(2*pi*knfreq*tkn/fs)+0.2525
kn_sig = 0.015*np.sin(2*pi*knfreq*tkn/fs) + 0.003
#kn_sig[:] = 0.0 # no coupling
#plt.plot(kn_sig) 
kn_sig = tile(kn_sig, (N,1))


# frequency modulation 
Nfft = 2048 # this is default for librosa
minfreq = 100
maxfreq = 2000.0
binspace = fs/maxfreq
totalbins = 2048//2
maxbin = np.int(maxfreq/binspace)
intrinsic_freqs = np.linspace(100, maxfreq, N) # nat rad freq range

t = np.arange(0,steps)
freqtraj = np.zeros((N,steps))
freqdev = 200 
freqrate = 2
for i, freq in enumerate(intrinsic_freqs):
    freqtraj[i,:] = freq + freqdev*sin(np.random.random()*pi + freqrate*t*2*np.pi/fs)

freqtraj = freqtraj/fs
model = Kuramoto(N, K/N, dt, nat_frequencies=freqtraj)

order_parameters = run(model, steps)
pyplot.plot(range(steps), order_parameters[0,:])
pyplot.title('phase coherence, R(t)')
pyplot.ylabel('sample')
pyplot.savefig('complex order params (finding sync).png', dpi=100)

#pyplot.plot(range(steps), order_parameters[1,:])
pyplot.show()

plt.plot(ph.T)

filename = 'output-audio/%r osc-model kn-freq %r.wav' %(N,knfreq)
print 'creating ', filename

def renderAudio(ph,N): 
    summedsines = np.sum(np.sin(ph), axis=0)
    summedsines = summedsines/N 
    summedsines = 0.67*summedsines/max(summedsines)
    librosa.output.write_wav(filename, summedsines, sr=fs)
    
renderAudio(ph, N)

y, sr = librosa.load(filename, sr=fs)
plt.figure()
D = librosa.amplitude_to_db(np.abs(librosa.stft(y)), ref=np.max)
librosa.display.specshow(D[0:300,:], sr=fs, y_axis='linear')
plt.savefig('output-audio/spectrum-plots/osc-model.png',dpi=80)




#!/usr/bin/env python

'''
KURA-ENSEMBLES.PY: ensembles of coupled oscillators 
author: Nolan Lem March 19 2019
can be run in 'normal' or 'pvoc' mode 

parameters are drawn from config file in ./config/config.py 

in 'normal' mode, the frequencies (stored in radfreq) are static throughout 
time and they must be of shape (1,N) where N is the num of oscillators (== numoscs)

in 'pvoc' mode, pvoc.py must be run first - then the radfreqs change at each sample based 
off of the phase vocoder analysis (generating instantaneous frequencies per frame, interpolated 
to the sample level). 
  also: numoscs = N//2 
   
 
'''
pvocflag = False # pvocflag != normalmode     
    

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import time
sys.path.insert(0, '/Users/nolanlem/Documents/kura/kura-python/config')
import config 
import pandas as pd
import librosa


sns.set()

pi = np.pi


def freq2rad(freqs, sr):
    freqs = freqs*2*np.pi/sr
    return freqs


def gaussian(numoscs, mu=0.00, sigma=0.1):
    #mu, sigma = 0.00, 0.1 # mean and std deviation 
    dist = np.random.normal(mu, sigma, numoscs);
    dist += 0.1;
    # sanity check the mean and variance 
    abs(mu - np.mean(dist)) < 0.01 
    abs(sigma - np.std(dist, ddof=1)) < 0.01 
    
    # generate histogram
    count, bins, ignored = plt.hist(dist, 30, density=True)
    plt.plot(bins, 1/(sigma*np.sqrt(2*np.pi))*np.exp(-(bins-mu)**2/(2*sigma**2)),
         linewidth=2, color='r')
    
    plt.show()
    return dist

def makeHistogram(intitial_freqs, evolved_freq, numbins, iters): 

    dist1 = pd.DataFrame(
            initial_freqs,
            columns=['intrinsic frequency distribution'])
    dist1.agg(['min','max','mean','std']).round(decimals=2)
    
    dist2 = pd.DataFrame(
            evolved_freq,
            columns=['entrained frequency distribution'])
    #dist2.agg(['min','max','mean','std']).round(decimals=2)
    
    fig1, ax1 = plt.subplots()
    dist1.plot.kde(ax=ax1, legend=False, title="histogram of intrinsic frequencies")
    dist1.plot.hist(normed=True, ax=ax1, bins=numbins)
    ax1.set_ylabel('probability')
    ax1.set_xlabel('radian freq')
    
    fig2, ax2 = plt.subplots()
    #dist2.plot.kde(ax=ax2, legend=False, title="histogram of evolved freq")
    #dist2.plot.hist(ax=ax2, bins=numbins, weights=np.ones(len(evolved_freq))/len(evolved_freq), density=False)
    dist2.plot.hist(ax=ax2, bins=numbins, normed=True, color=current_palette[0], title="entrained frequency distributions after %r iterations"%(iters)) 
    #ax2.set_xlim(-0.2,0.6)
    ax2.set_ylabel('probability')
    return dist1, dist2

def populateCircleMap(thephases):
    marks = np.empty(numoscs)
    marks.fill(100)
    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot(111)
    plt.scatter(np.cos(thephases), np.sin(thephases), facecolors='none', edgecolors='r', s=marks, alpha=0.1)
    ax.plot(np.sin(np.linspace(0,2*np.pi,100)), np.cos(np.linspace(0,2*np.pi,100)), linewidth='0.5', alpha=1.0)
    ax.set_xbound(lower=-1.2, upper=1.2)
    ax.set_ybound(lower=-1.2, upper=1.2)


def formGroups(numoscs, groups, k=0.5, static=True):
    # coupling matrix 
#    knmask = np.array([
#            [1,1,1,0,0], # group 1
#            [0,1,0,0,0], # group 2
#            [0,0,1,0,0],
#            [0,0,0,1,0],
#            [0,0,0,1,1]
#            ])
#    
#    # all-to-all coupling    
#    #knmask = np.ones((numoscs, numoscs))
#    
#    # grouping coupling coeffs for each group in knmask
#    kn = np.array([
#            [0.5, 0.5, 0.5, 0.0, 0.0],
#            [0, 0, 0, 0, 0],
#            [0, 0, 0, 0, 0],
#            [0, 0, 0, 0, 0],
#            [0, 0, 0, 0.5, 0.5]
#            ])   
    #mask is identity
    knmask = np.identity(numoscs)
    kn = np.arange(0.0, numoscs)/numoscs #(0..1) numoscs
    kn = np.random.random((numoscs,numoscs))
    # show coupling matrix    
    j = 0
    for i in range(0, numgroups):
        print i, j*oscgrp,(j+1)*oscgrp
        knmask[i, (j*oscgrp):((j+1)*oscgrp)] = 1
        j+=1
    
    # same coupling for each group
    if static==True:
        kn[:,:] = k
    
    # masked kns
    knmat = kn*knmask
    plt.imshow(knmask) 
    return kn, knmat, knmask

def randomGroups(numoscs):
    kn = -0.1 + np.random.random((numoscs,numoscs))*0.1
    knmask = np.random.randint(2, size=(numoscs,numoscs))
    knmat = kn*knmask 
    plt.imshow(knmask)
    return kn, knmat, knmask 
    

 

# freq of oscillators 
#fixedfreq = 0.5
#freq = np.empty(numoscs)
#freq.fill(fixedfreq) # freq is the intrinsic freq of each osc
# my initialized phases


# how many oscillators in ensemble
numoscs = config.config['numoscs']
p = 2*pi*np.random.rand(numoscs) # random initial phases
p = np.zeros(numoscs)
pang = np.zeros(numoscs)
#p = np.random.rand(numoscs)*0.0
kn = config.config['kn_start']
kn_end = config.config['kn_end'] 
mean = config.config['mean']
std = config.config['std']
kn_to_span = kn_end - kn
seconds = config.config['seconds']
sr = 8000

# sanity check from config.py file 
print("PARAMETERS: \n numoscs: {} kn: %r \n kn_end: %r mean: %r std: %r".format(numoscs, kn, kn_end, mean, std)) 
 
#freq = gaussian(numoscs, mean, std)
#freq = np.random.normal(mean, std, numoscs)
#initial_freqs = np.copy(freq)


#### example 
# N oscillators
sr = 8000
numoscs = 129 # if in "pvoc mode" (pvocflag == Tru), numoscs == N//2 in pvoc.py, otherwise, can choose number
totaliters = sr*seconds


# right now has to be divisor of numoscs ...
numgroups = 10
oscgrp = numoscs/numgroups
kn, knmat, knmask = formGroups(numoscs, numgroups, kn, static=True) 
#kn, knmat, knmask = randomGroups(numoscs)
r_m = np.zeros(numoscs) # temp array to store group's phase coherence, R 
r_mag = np.zeros((numoscs, totaliters)) # array to fill up phase coherence of each group over time

# instrinsic frequencies
# linear spacing 0 - 4k 
if pvocflag == False: 
    freq = np.linspace(0, sr/2, numoscs)
    radfreq = freq2rad(freq, sr)
    mags = np.ones((numoscs, totaliters))
if pvocflag == True: # if running in 'normal' mode
    # for use with pvoc.py
    radfreq = np.copy(freq)
    # mags = mags from pvoc.py
  
print radfreq.shape



# randomize initial phases of each osc
p = np.random.random((numoscs,numoscs))

phases = np.zeros((numoscs, totaliters))

pang = np.zeros((numoscs,numoscs))



# helper functions 
def accumulateGroupR(p, knmask, radfreq, it):
    #print 'pmat:\n', pmat  
    pmat = p*knmask # apply coupling mask to phases for group
    #print 'pmat:\n', pmat    
    i = 0
    for pgroup, kgroup in zip(pmat, knmask):
        activegrp = pgroup[np.nonzero(kgroup)] # get nonzero elements in masked phase array
        numgroup = activegrp.shape[0] # number in group
        rang, rmag = accumulateR(activegrp, numgroup) # gen complex order params per group        
        r_m[i] = rmag   # tmp store phase coherence for each group         
        # apply group COP to oscillators      
        pang[i,:] = kgroup*kn[i]*rmag*np.sin(rang - pgroup) # pang[i,:] is how much to shift phase of each oscillator       
        i += 1
    r_mag[:, it] = r_m # fill up phase coherence for each group timestep for plotting
    #print 'pang \n', pang
    coupling = np.sum(pang, axis=0)
    #print 'summed pang \n', coupling
    p = p + radfreq + coupling
    
    kn[:,:] += kn_to_span/totaliters
    
    return p


def accumulateR(ph, numoscs):
    r = np.sum(np.exp(ph*1j))   
    r = r/numoscs # divide by number of active oscillators
    rang = np.angle(r) # get avg phase angle, rang
    rmag = np.abs(r) # get phase coherence, rmag
    return [rang, rmag] # return complex order params

def updatePhases(ph, pang, orderparams, kn):
    # function to update phases of each oscillator in ensemble
    rang, rmag = orderparams
    pang = kn*rmag*np.sin(rang-ph)   
    ph = ph + freq + pang
    # return the phases
    return ph


# main loop 
for x in range(totaliters):
    print 'iter %r/%r'%(x, totaliters)
    # for pvoc
    if pvocflag == True:
        p = accumulateGroupR(p, knmask, radfreq[:,x], x)
    else:
        p = accumulateGroupR(p, knmask, radfreq, x)
               
    #print 'p:\n', np.sum(p, axis=0)/numoscs 
    ph = np.sum(p, axis=0)/numoscs
    phases[:, x] = ph
    #print phases


sines = np.sin(phases)*mags

    
summedsines = np.sum(sines, axis=0)
summedsines = 0.5*summedsines/max(summedsines) # normalize
librosa.output.write_wav("129osc-flute.wav", summedsines, sr=sr)
#plt.plot(phases)



#!/usr/env/bin python

import librosa 

import matplotlib.pyplot as plt 

import numpy as np

from librosa import display

# spectra 3:    ./outputaudio/5 oscs kn 0.05 to 0.2 mu 0.1 dev 0.1.wav.wav
# 5 oscs kn 0.05 to 0.2 mu 0.3 dev 0.111-59-23
y, sr = librosa.load("./outputaudio/5 oscs kn 0.05 to 0.2 mu 0.3 dev 0.111-59-23.wav", sr = 8000)

plt.figure(figsize=(12, 8))
D = librosa.amplitude_to_db(np.abs(librosa.stft(y)), ref=np.max)
librosa.display.specshow(D, y_axis='linear')
plt.colorbar(format='%+2.0f dB')
plt.title('Linear-frequency power spectrogram')
plt.ylim(0,4000)
plt.xlim(0,200)
plt.savefig("spectra7", format='png')